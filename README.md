# CAR BOOKING API

## Ejecutar

```console
mkdir .venv
pipenv shell
pipenv install
cd carbooking
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

Abrir `http://127.0.0.1:8000/api`, iniciar sesion y empezar a jugar con los endpoints disponibles