from rest_framework import viewsets
from rest_framework import permissions
from api.models import Client, Booking
from api.serializers import ClientSerializer, BookingSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class BookingViewSet(viewsets.ModelViewSet):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer

