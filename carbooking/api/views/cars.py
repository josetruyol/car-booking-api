from rest_framework import viewsets
from rest_framework import permissions
from django_filters import rest_framework as filters
from api.models import CarBrand, CarFeature, CarModel, Feature
from api.serializers import (
    CarBrandSerializer,
    CarFeatureSerializer,
    CarModelSerializer,
    FeatureSerializer,
)


class CarBrandViewSet(viewsets.ModelViewSet):
    queryset = CarBrand.objects.all()
    serializer_class = CarBrandSerializer


class FeatureViewSet(viewsets.ModelViewSet):
    queryset = Feature.objects.all()
    serializer_class = FeatureSerializer


class CarFeatureViewSet(viewsets.ModelViewSet):
    queryset = CarFeature.objects.all()
    serializer_class = CarFeatureSerializer


class CarModelFilter(filters.FilterSet):
    features = filters.ModelMultipleChoiceFilter(queryset=CarFeature.objects.all())

    class Meta:
        model = CarModel
        fields = ["features"]


class CarModelViewSet(viewsets.ModelViewSet):
    queryset = CarModel.objects.all()
    serializer_class = CarModelSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = CarModelFilter
