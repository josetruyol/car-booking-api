from rest_framework import serializers
from api.models import Client, Booking


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ("id", "name")


class BookingSerializer(serializers.ModelSerializer):
    def validate(self, data):
        car_model = data["car_model"]
        result = Booking.objects.filter(car_model=car_model)
        if len(result) >= car_model.units:
            raise serializers.ValidationError("No existen unidades disponibles")
        return data

    class Meta:
        model = Booking
        fields = ("id", "client", "car_model")

