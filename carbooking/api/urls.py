from django.urls import path, include
from rest_framework import routers
from . import views

app_name = "api"

router = routers.DefaultRouter()
router.register("car_brands", views.CarBrandViewSet)
router.register("features", views.FeatureViewSet)
router.register("car_features", views.CarFeatureViewSet)
router.register("car_models", views.CarModelViewSet)
router.register("clients", views.ClientViewSet)
router.register("bookings", views.BookingViewSet)

urlpatterns = [path("auth/", include("rest_framework.urls"))]
urlpatterns += router.urls
