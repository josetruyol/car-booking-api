from django.db import models
from django.utils.translation import gettext as _


class CarBrand(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = _("Car Brand")
        verbose_name_plural = _("Car Brands")

    def __str__(self):
        return self.name


class Feature(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.name}"


class CarFeature(models.Model):
    name = models.ForeignKey(Feature, related_name="models", on_delete=models.CASCADE)
    value = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.name}: {self.value}"


class CarModel(models.Model):
    name = models.CharField(max_length=100)
    brand = models.ForeignKey(CarBrand, related_name="models", on_delete=models.CASCADE)
    features = models.ManyToManyField(CarFeature, blank=True)
    units = models.IntegerField()

    def __str__(self):
        return self.name
