from django.db import models
from .cars import CarModel


class Client(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.name}"


class Booking(models.Model):
    client = models.ForeignKey(
        Client, related_name="bookings", on_delete=models.CASCADE
    )
    car_model = models.ForeignKey(
        CarModel, related_name="bookings", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.client} - {self.car_model}"
